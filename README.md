# SHA2:
It was designed for Hardware-Software Synthesis Project SHA-2.It consists of
implemention the SHA-256 function in hardware on Xilinx FPGA. Hardware implementation uses Quartz language to ensure its design specification. 

#The BSD 3-Clause License:
Copyright (c) 2013, Huseyin Hakan Pekmezci 
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    The name of the author may not be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY HUSEYIN HAKAN PEKMEZCI AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Installation
Heavily relies on Averest Quartz Framework and its prerequisities*. 
Current code was developed in Ubuntu Linux GNU (2.6.35-32-generic #67-Ubuntu SMP Mon Mar 5 19:35:26 UTC 2012 i686 GNU/Linux)

recent used modules list(in addition to the Framework base codes*):
UserModule.qrz
TestModule.qrz
BRAM_read_first.qrz

initially run the following command in Framework folder
$make test
then type in terminal
$cd Framework/usr/src

#Runtime
TestModule compiles aif file for all necessary modules within the code.
$qrz2aif TestModule.qrz

following command runs simulation in terminal:
$aif2trc -a TestModule.aifs

if text file output is required for the simulation:
$aif2trc -a -o output.txt Testmodule.aifs

qrz code transforming into verilog code:
$qrz2aif TestModule.qrz
$aif2verilog TestModule.aifs

xilinx ise tool can synthesise withing the same working directory:
$xilinx ise

#Recent simulation reports:
unoptimised version (version 0.9):
takes 1841 macro steps in aif2trc simulation for total 736bits data.

barely optimised version (version 0.9.4b)
takes 1972 amcro steps in aif2trc simulation for total 736bits data

#Recent synthesis reports:
unoptimised version (version 0.9):
around 3300Mhz frequency, nearly 303ns period time.

barely optimised version (version 0.9.4b)
around 98.727Mhz frequency, nearly 10ns period time.

#Notes:
*The abstract level of the algorithm does not change for both versions so it is called barely optimised version one for other. The BRAM usage and some small changes were done. 
*Keeping mind the consistent serial datain for algorithm was intended by usage of extra temporal variables. The major drawback was performance of the throughput suffered severely.
*Quartz framework was still in development when this work was undertaken therefore changing some segments of the code "to make it nicer" or "pointless usage of some statements" in aforementioned algorithm may cause some failures in simulation.
*The Framework base codes and Quartz toolkit were provided by University of Kaiserslautern,Computer Science, Embedded Systems group. Please have a look their licence conditions before doing any arrangements.
